
public class Test {
	
	//VARIABLES
	private String name;	   
	private static String staticStr = "STATIC-STRING";
	
	//CONSTRUCTOR
	public Test(String n){
	        this.name = n;
	}
		    
	//STATIC
    public static void testStaticMethod(){
        System.out.println("Hey... I am in static method...");
        System.out.println(Test.staticStr);
    }
     
    // PUBLIC
    public void testObjectMethod(){
        System.out.println("Hey i am in non-static method");
        System.out.println(this.name);
 
    }
     
}
